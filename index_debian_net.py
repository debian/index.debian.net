#!/usr/bin/env python3
"""
index.debian.net service
Copyright 2024 Federico Ceratto <federico@debian.org>
Released under AGPL-3+
"""

from datetime import datetime as dt
from logging import getLogger
from time import monotonic, time, sleep
from threading import Thread
import logging
import json

from bottle import Bottle, run, template  # debdeps: python3-bottle
from debiancontributors import Identifier  # debdeps: python3-debiancontributors
from debiancontributors import submission as sub
from systemd.journal import JournalHandler  # debdeps: python3-systemd
from ldap3 import Server, Connection, SUBTREE  # debdeps: python3-ldap3
from systemd import journal  # debdeps: python3-systemd
import requests

conf = {}
log = getLogger()
log.addHandler(JournalHandler(SYSLOG_IDENTIFIER="index_debian_net"))
log.setLevel(logging.DEBUG)
app = Bottle()

CONF_FN = "/etc/index_debian_net.conf"

LDAP_URI = "ldaps://db.debian.org/"
LDAP_INTERVAL = 600
PROM_URI = "https://prometheus.debian.net/api/v1/query?query=avg_over_time(probe_success{job=%22icmp%22}[1m])"
PROM_INTERVAL = 60
LDAP_URI = "ldaps://db.debian.org:636"
BASE_DN = "dc=debian,dc=org"
SEARCH_FILTER = "(dnsZoneEntry=*)"

TPL = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index.debian.net</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/css/theme.default.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>
</head>
<body>
    <style>
      html {
        margin: 0;
      }
      body {
        margin: 0;
      }
      header {
        border-bottom: 1px solid #999;
        display: block;
        text-align: center;
        width: 100%;
      }
      article {
        padding: 1em 3em 3em 3em;
        color: #444;
      }
      ul li {
        margin: 1em;
      }
      header a {
        text-decoration: none;
      }
    </style>
    <header>
      <p>
        <a href="https://salsa.debian.org/debian/index.debian.net">index.debian.net</a>
        - released under AGPLv3
      </p>
    </header>
    <article>

        <p>This page lists .debian.net services. It fetches data from <a href="https://db.debian.org/">db.debian.org</a>
        every 10 minutes and from <a href="https://prometheus.debian.net">prometheus.debian.net</a> every minute.
        <br/>
        FQDNs are slightly obfuscated as e.g. "index.d.n" to discourage bots.
        <br/>
        See <a href="https://wiki.debian.org/Services/">https://wiki.debian.org/Services/</a> for a hand-maintained list
        and <a href="https://wiki.debian.org/Teams/DebianNet">https://wiki.debian.org/Teams/DebianNet</a> for more
        information.
        <br/>
        The data is also used to update <a href="https://contributors.debian.org/source/debian.net%20service/">contributors.debian.org</a>
        </p>

        <table id="services" class="tablesorter">
            <thead>
                <tr>
                    <th>FQDN</th>
                    <th>Ping status</th>
                    <th>Last update</th>
                    <th>Owner Login</th>
                    <th>Owner Name</th>
                    <th>On LDAP</th>
                    <th>On Prometheus</th>
                </tr>
            </thead>
            <tbody>
              % for row in table:
                <tr>
                  % for c in row:
                    <td>{{ c }}</td>
                  % end
                </tr>
              % end
            </tbody>
        </table>

        <script>
            $(document).ready(function() {
                $("#services").tablesorter();
            });
        </script>

    </article>
</body>
</html>

"""


last_prom_update = 0
last_ldap_update = 0


class Service:
    __slots__ = [
        "icmp_reachability",
        "on_prom",
        "on_ldap",
        "owner_uid",
        "owner_name",
        "last_update",
    ]

    def __init__(self):
        self.icmp_reachability = None
        self.on_prom = None
        self.on_ldap = None
        self.owner_uid = None
        self.owner_name = None
        self.last_update = None


# updates are performed without need for locking. Races are designed to be harmless here.
services: dict[str, Service] = {}


def update_ldap_data_if_needed():
    """Fetch public LDAP data and update global dict `services` when needed when needed."""
    global services
    global last_ldap_update
    if monotonic() - last_ldap_update <= LDAP_INTERVAL:
        return

    log.info("fetching ldap")
    server = Server(LDAP_URI, use_ssl=True)
    try:
        conn = Connection(server, auto_bind=True)
        conn.search(
            search_base=BASE_DN,
            search_filter=SEARCH_FILTER,
            search_scope=SUBTREE,
            attributes=[
                "uid",
                "cn",
                "sn",
                "dnsZoneEntry",
            ],
        )

        entries = conn.entries
        for e in entries:
            for dn in e.dnsZoneEntry:
                dn = dn.decode().split()[0]
                if dn not in services:
                    services[dn] = Service()

                s = services[dn]
                s.owner_uid = str(e.uid)
                s.owner_name = f"{e.cn} {e.sn}"
                s.on_ldap = True
                s.last_update = time()

    finally:
        conn.unbind()
        last_ldap_update = monotonic()


def update_prom_data_if_needed() -> None:
    """Fetch prometheus data and update global dict `services`"""
    global last_prom_update
    global services
    if monotonic() - last_prom_update <= PROM_INTERVAL:
        return

    resp = requests.get(PROM_URI, auth=(conf["prom_username"], conf["prom_password"]))
    resp.raise_for_status()
    j = resp.json()
    if j.get("status") != "success":
        raise ValueError()
    if j.get("data", {}).get("resultType") != "vector":
        raise ValueError()
    for e in j["data"]["result"]:
        dn = e["metric"]["instance"]
        if not dn.endswith(".debian.net"):
            continue
        dn = dn[:-11]
        reachability = float(e["value"][1])
        if dn not in services:
            services[dn] = Service()

        s = services[dn]
        s.on_prom = True
        s.icmp_reachability = reachability
        s.last_update = time()

    last_prom_update = monotonic()


def display_status(r) -> str:
    """Create status string"""
    if r is None:
        return ""

    if r > 0.8:
        return f"🟢 {r}"

    if r > 0.5:
        return f"🟡 {r}"

    return f"🔴 {r}"


def submit_contributions() -> None:
    """Submit data to contributors.debian.org"""
    update_ldap_data_if_needed()
    update_prom_data_if_needed()
    sub_api = sub.Submission("debian.net service", conf["contributors_token"])

    contributors_logins = set()
    for sv in services.values():
        if sv.owner_uid in contributors_logins:
            continue

        if not sv.on_ldap:
            continue

        if (time() - sv.last_update) > 3600 * 48:
            # not in LDAP nor prom anymore
            continue

        contributors_logins.add(sv.owner_uid)

    for uid in contributors_logins:
        ident = Identifier("login", uid)
        sub_api.add_contribution_data(ident, "service_maintainer")

    ok, info = sub_api.post()
    if ok:
        log.info("Contributions sent")

    else:
        log.error("Failed contribution upload for %s: %s", uid, info)


def run_contributors_updater() -> None:
    """Refresh data and send it to contributors.debian.org once a day (approx)"""
    log.info("Starting contribution updater")
    while True:
        sleep(3600 * 24)
        try:
            submit_contributions()
        except Exception:
            log.error("Failed", exc_info=True)


@app.route("/")
def index():
    """HTTP entry point at /
    Fetches fresh data as needed
    """
    update_ldap_data_if_needed()
    update_prom_data_if_needed()

    table = [
        (
            dn + ".d.n",
            display_status(s.icmp_reachability),
            dt.fromtimestamp(s.last_update).strftime("%Y-%m-%d %H:%M:%S"),
            s.owner_uid or "",
            s.owner_name or "",
            "✓" if s.on_prom else "",
            "✓" if s.on_ldap else "",
        )
        for dn, s in services.items()
    ]

    return template(TPL, table=table)


def main() -> None:
    """Load conf, run service"""
    global conf
    with open(CONF_FN) as f:
        conf = json.load(f)

    Thread(target=run_contributors_updater).start()

    log.info("Starting")
    run(app, host="localhost", port=8141, debug=True)


if __name__ == "__main__":
    main()
